# b:list

**b:list** is an android bucket list app that enables users to save bucket lists on the app.

### Built With
[Java](https://www.java.com/en), 
[SQLite](https://www.sqlite.org/index.html), 
[Android Studio](https://developer.android.com/studio), 
[Material Design](https://material.io/design), and
[Sketch](https://www.sketch.com/)

---

## Getting Started

**You must install Android Studio on your computer to run this project.**

The version of Android Studio should be 3.5.3 or higher.  
To clone the repository, type the following command on your Terminal or Command Prompt.  

``` git clone https://irene-kwon@bitbucket.org/irene-kwon/b-list.git ```

### Or Download APK
[Direct Download](https://drive.google.com/file/d/1_LoYn2hl2HGCR2_FpjBYWs2H6lkya_W8/view?usp=sharing)

---

## How to Use

1. Click **Login or Sign Up** buttons to get started.  
2. On the **To Do** tab, click the menu on the right side of the navbar to add your bucket list.
To remove all of your bucket lists, click **Remove All** on the menu.
3. To delete or complete a single bucket list item, simply click the item.
4. After the step 3, a pop-up box asking about deletion or completiton of an item will be shown.
5. You will see all completed items on the **Completed** tab.

Authentication is currently in development. Please check [Issues](https://bitbucket.org/irene-kwon/b-list/issues?status=new&status=open) for more information.

---

## License

[MIT License](LICENSE) is selected for this project since it allows anyone to use the project freely  
with conditions requiring preservation of copyright and license notices only.

---

## Contact

If you have any questions about **b:list**, do not hesitate to contact me at [Hkwon7327@conestogac.on.ca](mailto:Hkwon7327@conestogac.on.ca).
  